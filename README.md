# mkdocs container

mkdocsを手元で動かうためのコンテナイメージ

dockerが必要なので、インストールしていない場合はここを参考[（Windows）](https://hub.docker.com/editions/community/docker-ce-desktop-windows)[（macOS）](https://hub.docker.com/editions/community/docker-ce-desktop-mac)にインストールしてください。

※Linuxの場合は、お使いのディストリビューションのドキュメントを参照するなりしてください。

## 【推奨】コンテナの使い方（docker registoryを使う方法）

mkdocsを使っているリポジトリのmkdocs.ymlがあるディレクトリで、以下を実行してください。

```
docker run --rm -it -p 8000:8000 -v `pwd`:/docs registry.gitlab.com/comitia-tools/mkdocs-container
```

## コンテナの使い方（自分でビルドする方法）

このリポジトリをクローンして、以下を実行してください。

```
git clone https://gitlab.com/comitia-tools/mkdocs-container.git
cd mkdocs-container
docker build . -t mkdocs-container
```

その後、mkdocsを使っているリポジトリのmkdocs.ymlがあるディレクトリで以下を実行してください。

```
docker run --rm -it -p 8000:8000 -v `pwd`:/docs mkdocs-container
```

## 対応しているテーマ

- default
- material
