FROM python:3

WORKDIR /docs

# Install mkdocs and mkdocs-material
RUN pip install --upgrade pip \
    && pip install mkdocs mkdocs-material

EXPOSE 8000

# Start development server by default
ENTRYPOINT ["mkdocs"]
CMD ["serve", "--dev-addr=0.0.0.0:8000"]